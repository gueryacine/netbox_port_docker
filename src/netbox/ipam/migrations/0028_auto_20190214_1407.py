# Generated by Django 2.1.7 on 2019-02-14 14:07

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [("ipam", "0027_port_mode")]

    operations = [
        migrations.AddField(
            model_name="port",
            name="tagged_vlans",
            field=models.ManyToManyField(
                blank=True, related_name="roles_as_tagged", to="ipam.VLAN"
            ),
        ),
        migrations.AddField(
            model_name="port",
            name="untagged_vlan",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                related_name="roles_as_untagged",
                to="ipam.VLAN",
            ),
        ),
        migrations.AlterField(
            model_name="port",
            name="types",
            field=models.PositiveSmallIntegerField(default=1, null=True),
        ),
    ]
