# Generated by Django 2.1.7 on 2019-02-25 08:00

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [("ipam", "0028_auto_20190214_1407")]

    operations = [
        migrations.AlterModelOptions(
            name="port",
            options={
                "ordering": ["site", "group", "vid"],
                "verbose_name": "Port-template",
                "verbose_name_plural": "Port-templates",
            },
        ),
        migrations.AlterModelOptions(
            name="portgroup",
            options={
                "ordering": ["site", "name"],
                "verbose_name": "Port-templates group",
                "verbose_name_plural": "Port-templates groups",
            },
        ),
    ]
