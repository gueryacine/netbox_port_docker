#!/bin/bash


minikube start

kubectl apply -f netbox-namespace.yaml 
sleep 1
kubectl apply -f configmap.yaml --namespace netbox
sleep 1
kubectl apply -f postgres.yaml --namespace netbox
sleep 1
kubectl apply -f deployment.yaml --namespace netbox
sleep 1
kubectl apply -f service.yaml --namespace netbox
sleep 1
kubectl apply -f ingress.yaml --namespace netbox

kubectl get pods -n netbox

minikube ip